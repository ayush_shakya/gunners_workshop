# README #

This project simulates an Agile sprint. Let's assume we are in the middle of a sprint meeting, and the objective has been agreed.
We aim to finish up sprint planning, and go through a few simulated scrums (all within a day) - recommend 10:30, 13:00, 15:30.

## Sprint Objective ##
Create the landing page for a responsive/adaptive website

## Features ##
In the Sprint Planning meeting, the following features have been prioritised for this sprint.

### Elements position: ###
1. T+A Logo, Sign Up, Sign In >> Top left  
2. Search input >> Top right  
3. Header (frosted glass effect) >> Top  
4. Phone Image >> Centre Left.  
5. Copy >> Centre Right.  
6. Nav >> Right  
7. No scrollbars on the page  
8. Background always cover entire screen without stretching  
    
### Animations (scroll): ###
1. On scroll up/down, the background image (and only background) moves up/down.  
2. During transition from one background to the other, scrolling is disabled until background comes to a rest.  
3. Phone screen colour tint transitions as screen changes.  
4. Old copy fades out and new copy fades in during transition.  
5. Nav buttons should always reflect the correct screen shown.  
  
### Animations (others): ###
1. Sign Up, Sign In mouseover effects.  
    
*Button behaviours, search input behaviours are all part of a future sprint.

### Additonal animations for mobile: ###
1. Sidebar slides in/out.  
2. Phone moves right as sidebar slides in.  
3. When sidebar is opened, tapping outside the sidebar pushes sidebar back to docked position.  
4. In many default mobile devices, whenever user taps on an input field on a mobile, the browser will zoom into it. Disable that.  
5. Alternatively, implement it such that when user focuses on input, zoom into it. Upon filling data and closing the keypad, get it back to full size.  


Desktop layout (width: 1024px & above), Mobile layout (width: 1023px & below).

## Your job: ##
1.  Finish sprint planning:
    * Discuss and break down these features into tasks.
    * Scrum Master: add these tasks as issues.
    
2. Hold 3 scrum meetings:
    * In each scrum, team members to take on the issues (remember to assign on BitBucket).
    * At end of each scrum, decide time for the next scrum meeting.

## Remember ##
* Scrum Meeting Report:
    1. What did I do?
    2. What problems did I face?
    3. What will I do?

* Scrum Master:
    1. Set a timer for sprint (keep sprint meeting within 20-30 min).
    2. Set a timer for scrum (scrum should be NO MORE THAN 15 min).
    3. Set alarm for next sprint, and assemble team for scrum.

* Respect time: when it's time for scrum, pause working and report progress.
