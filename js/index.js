$(document).ready(function(){
	$("#stage").height(window.innerHeight);
	$("#show1").height(window.innerHeight);
});
$('#paginator > li').click(function() {
	$('#paginator > li').removeClass('active');
	$(this).addClass('active');
	var slider = $(this).attr('data-id');
	$("#stage").height(window.innerHeight);
	$("#" + slider).height(window.innerHeight);
	$("#" + slider).find('.screen').css({'background-color':'255,102,0,0.25'});
	$('#stage').scrollTo($("#" + slider));
});